@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lista de Clientes</div>


                <div class="col text-right">
                    <a href="{{ route ('list.clientes') }}" class="btn btn-sm btn-success"">Cancelar</a>
                </div>


                <div class="card-body">

                    <form role="form" methods="POST" action="{{route('guardar.clientes')}}">
                         {{ csrf_field() }}
                         {{ method_field('post') }}
                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Nombres</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Apellidos</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Cédula</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Dirección</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Teléfono</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Fecha de Nacimiento</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-lg-8">  
                            <label class="from-control-label" for="nombre">Email</label>
                            <input type="text" class="from-control" name="Nombre del Cliente">
                            </div>
                        </div> 

                        <button type="submit" class="btn btn-success pull-right">guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection