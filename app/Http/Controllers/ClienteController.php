<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ListaCliente(Request $request)
    {
        
        $cliente = Cliente::all();
        //dd($cliente);
        return view('Clientes.listaClientes')->with('Cliente', $cliente);
    }

    public function CrearAgregarCliente(Request $request) {

        $cliente = Cliente::all();
        return view('Clientes.crearClientes')->with('Cliente', $cliente);
    }

    public function GuardarCliente(Request $request) {
        $this->validate($request, [
        'Nombres' => 'required',
        'Apellidos' => 'required',
        'Cédula' => 'required',
        'Dirección' => 'required',
        'Teléfono' => 'required',
        'Fecha_Nacimiento' => 'required',
        'Email' => 'required',
        ]);

        $cliente = new Cliente;
        $cliente->Nombres = $request->Nombres;
        $cliente->Apellidos = $request->Apellidos;
        $cliente->Cédula = $request->Cédula;
        $cliente->Dirección = $request->Dirección;
        $cliente->Teléfono = $request->Teléfono;
        $cliente->Fecha_Nacimiento = $request->Fecha_Nacimiento;
        $cliente->Email = $request->Email;

        $cliente->save();
        return redirect()->route('list.clientes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
