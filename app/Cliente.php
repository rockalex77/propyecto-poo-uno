<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'Nombres',
        'Apellidos',
        'Cédula',
        'Dirección',
        'Teléfono',
        'Fecha_Nacimiento',
        'Email',
    ];
}
